#include "server_config.h"

UA_String String_2_UA_String(char *src);


UA_String String_2_UA_String(char *src) 
{

    UA_String s; 
    s.length = 0; 
    s.data = NULL;

    if(!src)
        return s;
    s.length = strlen(src);
    if(s.length > 0) {
        s.data = (UA_Byte*)UA_malloc(s.length);
        if(!s.data) {
            s.length = 0;
            return s;
        }
        memcpy(s.data, src, s.length);
        // s.length-=1;
    } else {
        s.data = (UA_Byte*)UA_EMPTY_ARRAY_SENTINEL;
    }
    return s;
}


UA_StatusCode OPC_UA_Server_LoginPassword (UA_Server *server,char* username, char* password)
{
    UA_UsernamePasswordLogin logins[1]; 

    logins->username = String_2_UA_String(username);     //username
    logins->password = String_2_UA_String(password);     //Password   

    UA_ServerConfig *s_config = UA_Server_getConfig(server);
    s_config->accessControl.clear(&s_config->accessControl);
    UA_StatusCode retval = UA_AccessControl_default(s_config, false,
             &s_config->securityPolicies[s_config->securityPoliciesSize-1].policyUri, 
             1, logins);

    return retval;

}

void OPC_UA_EndPoint(UA_ServerConfig *config)
{
    char* test = "esp32";
    UA_EndpointDescription end_point;
    config->endpointsSize = 1;
    config->endpoints->endpointUrl = String_2_UA_String(test);
    
    // createEndpoint(config, end_point,
    //                String_2_UA_String("http://opcfoundation.org/UA/SecurityPolicy#Basic256Sha256"),
    //                UA_MESSAGESECURITYMODE_SIGNANDENCRYPT);
    UA_ServerConfig_addEndpoint(config, 
                                String_2_UA_String("http://opcfoundation.org/UA/SecurityPolicy#Basic256Sha256"),
                                UA_MESSAGESECURITYMODE_SIGNANDENCRYPT);
}




UA_StatusCode OPC_UA_Server_Encryption (UA_Server *server, OPC_UA_Security_et security)
{
    UA_ServerConfig *s_config = UA_Server_getConfig(server);
    
    char* certificate = CERTIFICADO;
    char* privateKey  = PRIVATE_KEY;

    size_t length_certificate = (sizeof(CERTIFICADO)/sizeof(char));
    size_t length_private_key = (sizeof(PRIVATE_KEY)/sizeof(char));

    ESP_LOGW("Encrypt", "Length_Certificate(%zu) Length_Private_Key(%zu)",length_certificate,length_private_key);

    UA_String certificado_STRING;
    UA_String key_STRING;
    // UA_String teste;
    // teste.length = 5;
    // teste.data = "teste";

    certificado_STRING.length = length_certificate;
    certificado_STRING.data = pvPortMalloc(sizeof(UA_Byte)*length_certificate);
    memcpy(certificado_STRING.data, certificate,certificado_STRING.length-1);

    key_STRING.length = length_private_key;
    key_STRING.data = pvPortMalloc(sizeof(UA_Byte)*length_private_key);
    memcpy(key_STRING.data, privateKey, key_STRING.length-1);

    UA_StatusCode retval = UA_STATUSCODE_GOOD;    

    switch (security)
    {
    case BASIC128RSA15:
        // s_config->securityPolicies;
        // s_config->securityPolicyUri = UA_STRING_ALLOC("http://opcfoundation.org/UA/SecurityPolicy#Basic128Rsa15");
        // UA_StatusCode retval   = UA_SecurityPolicy_Basic128Rsa15(s_config, cetificate, 0);
        break;
    
    case BASIC256:
        // UA_SecurityPolicy_Basic256();
        break;
    
    case BASIC256SHA256:
        
    
        // s_config->applicationDescription.applicationType = UA_APPLICATIONTYPE_SERVER;
        // s_config->endpoints;
        // s_config->endpointsSize = 1;
        //s_config->securityPoliciesSize = 1;
        //s_config->securityPolicies->policyUri = UA_STRING("http://opcfoundation.org/UA/SecurityPolicy#Basic256Sha256");
        //s_config->securityPolicies->localCertificate = certificado_STRING;
        
        retval = UA_ServerConfig_setDefaultWithSecurityPolicies(s_config, 
                                                                4840, 
                                                                &certificado_STRING,
                                                                &key_STRING,
                                                                0,//trustlist,
                                                                0,//trustlistSize,
                                                                0,//issuerList,
                                                                0,//issuerListSize,
                                                                0,//revocationList,
                                                                0);//revocationListSize);
        if (retval != UA_STATUSCODE_GOOD)
        {
            ESP_LOGE("Conifgure security policie","Retval ERROR  %d", retval);
        }
        break;
    
    default:
        // ESP_LOGW(TAG, "The Security Don't Have Be Defined. The Cryptography is none");
        break;
    }

    free(certificado_STRING.data);
    free(key_STRING.data);
    //UA_StatusCode retval = 0;
    return retval;
}




