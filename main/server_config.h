#include <stdio.h>
#include "open62541.h"


#define CERTIFICADO "-----BEGIN CERTIFICATE-----MIIDwzCCAqugAwIBAgIUamo7l+8082/AE9s1B6oadrHbti0wDQYJKoZIhvcNAQELBQAwcTELMAkGA1UEBhMCVVMxCzAJBgNVBAgMAk5ZMQswCQYDVQQHDAJOWTEVMBMGA1UECgwMT3JnYW5pemF0aW9uMRkwFwYDVQQLDBBPcmdhbml6YXRpb25Vbml0MRYwFAYDVQQDDA0xOTIuMTY4LjAuMTAzMB4XDTIxMDUxMTIwMTAzOFoXDTIxMDYxMDIwMTAzOFowcTELMAkGA1UEBhMCVVMxCzAJBgNVBAgMAk5ZMQswCQYDVQQHDAJOWTEVMBMGA1UECgwMT3JnYW5pemF0aW9uMRkwFwYDVQQLDBBPcmdhbml6YXRpb25Vbml0MRYwFAYDVQQDDA0xOTIuMTY4LjAuMTAzMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAt5QDhreLNwMMcWD4LCbjrUHdhzuhC4jxHwqd/+lxxL0l8oT4SPm5Qwc9VokEPYiURSlFfE7dDffWJNyAwksK4JSEY8TukOJmS/nQ0RmcZQEvDVTX93rQS62mG/pbsY6YZPE8IBVS5UbbHQd1YKcFUbv8VlpJN/LAyYqRnEOIyF+gUVF0EX4c14uB7puUohrJusQ3DAP9PMvS85tEP6NhTER4QWBL69m4n0B71Iwok2PWUlAImWaL81hu8dujyqueKjtFJun4M1Jy/3xxveL0AvyIW2h6q6z22r08SZ6VaX1ZfXQ4lIzw59qBB70/xSIkh2ShohpkruMBdqEyZEGDnwIDAQABo1MwUTAdBgNVHQ4EFgQUZUgf7trSJ/oYJPZ7RCMuH+Ux3UYwHwYDVR0jBBgwFoAUZUgf7trSJ/oYJPZ7RCMuH+Ux3UYwDwYDVR0TAQH/BAUwAwEB/zANBgkqhkiG9w0BAQsFAAOCAQEApSJ8VduRy1KaSS5H5eNDyqpi1tzeulyLg3yqMbpv+2bZ2fIY0m+44o5QkCZn5u8nfvxqej7xNcG/afRzElitsTvS6edidEazuK1tTdw8u31zbRxn9zZKp9GD7HGTk6XJEvMyMu5HEimAY4gVNpDiUBd60x8D84k1x/kW9o2olIZN7SKa5oYVBoZUbSBNlibXLPPyEuwczv2uI5dj1x6QQIwUbO+T8z/SrQmRlUO20KUGVvnHokZTNPr+OjpvgFrWiDpDOA9Cx0k8dsMNSBozZP3nQjd9KhkZVXAQeeT2JMbAjUHyyNVsI7DHVz190NF4tCqLs+rB45QtTjSqK8vITg==-----END CERTIFICATE----"
#define PRIVATE_KEY "-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwh/R4lAnykijzuDdsWpZLlZdLZhFDIyDn3FWHpP/7Y4TymQJgToGa1RTUuuxZd0ewZolgwz7ZJCCdhGDGD7ek3lGi9VIK3jhUe3do0hAoc8hUMu+V977O5DIfTYBbaYzGtiyaRji7qp4bz/4SOY7CBFGXCX0Gj4TFoYuuDbsuplawAeJZmodlenjNyr91W8clHADnjdWOOPktQrvAE8TS+K6l8LXH44V7fsCmeKTc5Jocxsa58NJao0MYZu8FlvHSn840EzYbS+SuXToJZFcg8x+7Ll1jZkPlG/Hq67slgo3YDTH1YGV2oKHDcEZixczT/NdMx6ms1UwLOehjmYZJQIDAQAB-----END PUBLIC KEY-----"


typedef enum
{
    NONE = 0,
    BASIC128RSA15,
    BASIC256,
    BASIC256SHA256
}OPC_UA_Security_et;

/**
 * If don't call this function, the login and password don't will be necessary
 * to communitace the master OPC UA with Esp32 (Slave OPC UA)
 * 
 * Obs.: Just use this function later use "UA_ServerConfig_setMinimalCustomBuffer" fuction
*/
UA_StatusCode OPC_UA_Server_LoginPassword (UA_Server *server,
                                           char* login,
                                           char* password);

void OPC_UA_EndPoint(UA_ServerConfig *config);

UA_StatusCode OPC_UA_Server_Encryption (UA_Server *server, 
                                        OPC_UA_Security_et security);

