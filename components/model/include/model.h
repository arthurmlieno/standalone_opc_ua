#include "open62541.h"

/* GPIO Numbers */

typedef enum
{
    BOOL = 0,
    INT8,
    UINT8,
    INT16,
    UINT16,
    INT32,
    FLOAT
} UA_Var_Type_et;

typedef enum
{
    READ = 0,
    READ_WRITE
} UA_Data_Access_Level_et;

typedef struct
{
    uint16_t   nsIndex;		    //nsIndex
	char*      name;			//name of node
	uint8_t	   action;			//0 -> read       1->write
} OPC_Read_Event_st;

void CreateDataSourceVariable(UA_Server *server, uint16_t nsIndex, 
                              char* name, UA_Var_Type_et Var_Type, void* Value, 
                              UA_Data_Access_Level_et Data_Access_Level);


void WriteDataSourceVariable(UA_Server *server, uint16_t nsIndex, 
                             char* name, UA_Var_Type_et Var_Type,  
                             void* Value);

void ReadDataSourceVariable(UA_Server *server, uint16_t nsIndex, char* name, 
                            UA_Var_Type_et Var_Type, void *outValue);

bool OPC_Read_Event_Handler(OPC_Read_Event_st *out_data);

/////////////////////////////////////////////////////////////////////////
void Call_Create_Data_Source(UA_Server *server);