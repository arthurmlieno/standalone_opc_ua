#include "open62541.h"
#include "model.h"

#define Tag "Model"

void CreateDataSourceVariable(UA_Server *server, uint16_t nsIndex, 
                              char* name, UA_Var_Type_et Var_Type, void* Value,
                              UA_Data_Access_Level_et Data_Access_Level) 
{
    
    UA_VariableAttributes attr = UA_VariableAttributes_default;
    
    switch (Data_Access_Level)
    {
    case READ:
        attr.accessLevel = UA_ACCESSLEVELMASK_READ;
        break;
    
    case READ_WRITE:
        attr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;
        break;
    default:
        break;
    }


    switch (Var_Type)
    {
    case BOOL:
        UA_Variant_setScalarCopy(&attr.value, (UA_Boolean*) Value, &UA_TYPES[UA_TYPES_BOOLEAN]);
        break;
    
    case INT8:
        UA_Variant_setScalarCopy(&attr.value, (UA_SByte*) Value, &UA_TYPES[UA_TYPES_SBYTE]);
        break;

    case UINT8:
        UA_Variant_setScalarCopy(&attr.value, (UA_Byte*) Value, &UA_TYPES[UA_TYPES_BYTE]);
        break;

    case INT16:
        UA_Variant_setScalarCopy(&attr.value, (UA_Int16*) Value, &UA_TYPES[UA_TYPES_INT16]);
        break;
    
    case UINT16:
        UA_Variant_setScalarCopy(&attr.value, (UA_UInt16*) Value, &UA_TYPES[UA_TYPES_UINT16]);
        break;

    case INT32:
        UA_Variant_setScalarCopy(&attr.value, (UA_Int32*) Value, &UA_TYPES[UA_TYPES_INT32]);
        break;

    case FLOAT:
        UA_Variant_setScalarCopy(&attr.value, (UA_Float*) Value, &UA_TYPES[UA_TYPES_FLOAT]);
        break;

    default:
        break;
    }
    
    attr.description = UA_LOCALIZEDTEXT_ALLOC("en-US","the answer");
    attr.displayName = UA_LOCALIZEDTEXT_ALLOC("en-US","the answer");
    UA_NodeId myIntegerNodeId = UA_NODEID_STRING_ALLOC(nsIndex, name);
    UA_QualifiedName myIntegerName = UA_QUALIFIEDNAME_ALLOC(nsIndex, name);
    UA_NodeId parentNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);
    UA_NodeId parentReferenceNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
    UA_Server_addVariableNode(server, myIntegerNodeId, parentNodeId,
                                                            parentReferenceNodeId, myIntegerName,
                                                            UA_NODEID_NULL, attr, NULL, NULL);
}


void WriteDataSourceVariable(UA_Server *server, uint16_t nsIndex, 
                             char* name, UA_Var_Type_et Var_Type, 
                             void* Value) 
{
    UA_NodeId myNodeId = UA_NODEID_STRING(nsIndex, name);
    UA_Variant myVar;
    UA_Variant_init(&myVar);

    switch (Var_Type)
    {
        case BOOL:
            UA_Variant_setScalar(&myVar, (UA_Boolean*) Value, &UA_TYPES[UA_TYPES_BOOLEAN]);
            break;
        
        case INT8:
            UA_Variant_setScalar(&myVar, (UA_SByte*) Value, &UA_TYPES[UA_TYPES_SBYTE]);
            break;

        case UINT8:
            UA_Variant_setScalar(&myVar, (UA_Byte*) Value, &UA_TYPES[UA_TYPES_BYTE]);
            break;

        case INT16:
            UA_Variant_setScalar(&myVar, (UA_Byte*) Value, &UA_TYPES[UA_TYPES_INT16]);
            break;
        
        case UINT16:
            UA_Variant_setScalar(&myVar, (UA_UInt16*) Value, &UA_TYPES[UA_TYPES_UINT16]);
            break;

        case INT32:
            UA_Variant_setScalar(&myVar, (UA_Int32*) Value, &UA_TYPES[UA_TYPES_INT32]);
            break;

        case FLOAT:
            UA_Variant_setScalar(&myVar, (UA_Float*) Value, &UA_TYPES[UA_TYPES_FLOAT]);
            break;

        default:
            break;
    }
    
    UA_Server_writeValue(server, myNodeId, myVar);
}


void ReadDataSourceVariable(UA_Server *server, uint16_t nsIndex, char* name, 
                            UA_Var_Type_et Var_Type, void *outValue)
{
    UA_NodeId myNodeId = UA_NODEID_STRING(nsIndex, name);
    UA_Variant *out;

    out = (UA_Variant*) pvPortMalloc(sizeof(UA_Variant));

    UA_Server_readValue(server, myNodeId, out);
     
    

    switch (Var_Type)
    {
        case BOOL:
            printf("Value in ns:%u;s=%s is: %u\n",nsIndex, name,*(bool*) out->data);
            memcpy(outValue, out->data, sizeof(bool));
            break;
        
        case INT8:
            printf("Value in ns:%u;s=%s is: %i\n",nsIndex, name,*(int8_t*) out->data);
            memcpy(outValue, out->data, sizeof(int8_t));
            break;

        case UINT8:
            printf("Value in ns:%u;s=%s is: %u\n",nsIndex, name,*(uint8_t*) out->data);
            memcpy(outValue, out->data, sizeof(uint8_t));
            break;

        case INT16:
            printf("Value in ns:%u;s=%s is: %i\n",nsIndex, name,*(int16_t*) out->data);
            memcpy(outValue, out->data, sizeof(int16_t));
            break;
        
        case UINT16:
            printf("Value in ns:%u;s=%s is: %u\n",nsIndex, name,*(uint16_t*) out->data);
            memcpy(outValue, out->data, sizeof(uint16_t));
            break;

        case INT32:
            printf("Value in ns:%u;s=%s is: %i\n",nsIndex, name,*(int32_t*) out->data);
            memcpy(outValue, out->data, sizeof(int32_t));
            break;

        case FLOAT:
            printf("Value in ns:%u;s=%s is: %f\n",nsIndex, name,*(float*) out->data);
            memcpy(outValue, out->data, sizeof(float));
            break;

        default:
            break;
    }
}


bool OPC_Read_Event_Handler(OPC_Read_Event_st *out_data)
{
    bool verify = 0;
    OPC_UA_Action_In_Socket_st *get_data;
    get_data = (OPC_UA_Action_In_Socket_st*) pvPortMalloc(sizeof(OPC_UA_Action_In_Socket_st));
    verify = Get_Data_Queue_OPCSocket(get_data);
    if(verify)
    {
        out_data->nsIndex = (uint16_t*) get_data->nsIndex;
        out_data->name = (char*) get_data->name;
        out_data->action = (uint8_t*) get_data->action;
        free(get_data);
        return verify;
    }
    free(get_data);
    return verify;
}


///////////////////////////////////////////////////////////////////////////////////
void Call_Create_Data_Source(UA_Server *server)
{
    void *data_out;

    data_out = (void*) pvPortMalloc(sizeof(uint8_t)*4);


    uint8_t b_teste = 1;
    CreateDataSourceVariable(server, 1, "Bool", BOOL, &b_teste, READ_WRITE);
    ReadDataSourceVariable  (server, 1, "Bool", BOOL, data_out);
    

    int8_t B_teste = 8;
    CreateDataSourceVariable(server, 1, "Int8", INT8, &B_teste, READ_WRITE);
    ReadDataSourceVariable  (server, 1, "Int8", INT8, data_out);


    uint8_t UB_teste = 9;
    CreateDataSourceVariable(server, 1, "UInt8", UINT8, &UB_teste, READ_WRITE);
    ReadDataSourceVariable  (server, 1, "UInt8", UINT8, data_out);


    int16_t I16_teste = 16;
    CreateDataSourceVariable(server, 1, "Int16", INT16, &I16_teste, READ_WRITE);
    ReadDataSourceVariable  (server, 1, "Int16", INT16, data_out);

    
    uint16_t UI16_teste = 17;
    CreateDataSourceVariable(server, 1, "UInt16", UINT16, &UI16_teste, READ_WRITE);
    ReadDataSourceVariable  (server, 1, "UInt16", UINT16, data_out);

    
    int32_t Int32 = 42;
    CreateDataSourceVariable(server, 1, "the.answer", INT32, &Int32, READ);
    ReadDataSourceVariable  (server, 1, "the.answer", INT32, data_out);

    
    float f_teste = 25.5;
    CreateDataSourceVariable(server, 1, "temperature", FLOAT, &f_teste, READ);
    ReadDataSourceVariable  (server, 1, "temperature", FLOAT, data_out);
    f_teste = 21.1;
    WriteDataSourceVariable(server, 1, "temperature", FLOAT,  &f_teste);
    ReadDataSourceVariable (server, 1, "temperature", FLOAT, data_out);

    printf("Value in ns:%u;s=%s is: %f\n",1, "temperature",*(float*) data_out);
    free(data_out);
}